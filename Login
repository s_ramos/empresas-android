package com.example.exemplo.ioasys_project;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.exemplo.ioasys_project.interfaces.Log_app;
import com.example.exemplo.ioasys_project.modelos.TokenRequest;
import com.example.exemplo.ioasys_project.modelos.TokenResponse;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Login extends AppCompatActivity {

    private EditText email,senha;
    private Button entrar;
    private Log_app service;
    public static final String API_BASE_URL = "http://54.94.179.135:8090/api/v1/";
    private static final String PREF_NAME = "LoginChefPreferences";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = (EditText)findViewById(R.id.editText);
        senha = (EditText)findViewById(R.id.editText2);
        entrar = (Button)findViewById(R.id.button);

        entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (email.getText().toString().isEmpty() || senha.getText().toString().isEmpty()){

                    Toast.makeText(getApplicationContext(),"Preencha todos os campos.",Toast.LENGTH_LONG).show();
                }

                else if(!isEmail(email.getText().toString())){

                    Toast.makeText(getApplicationContext(),"Digite um email válido.",Toast.LENGTH_LONG).show();
                }
                else if (isPassword(senha.getText().toString())){

                    Toast.makeText(getApplicationContext(),"Senha com menos de 5 carateres.",Toast.LENGTH_LONG).show();
                }

                else{

                    Retrofit retrofit = new Retrofit.Builder().baseUrl(API_BASE_URL).
                            addConverterFactory(GsonConverterFactory.create())
                            .build();
                    service = retrofit.create(Log_app.class);

                     TokenRequest tokenRequest = new TokenRequest();

                     tokenRequest.setEmail(email.getText().toString());
                     tokenRequest.setPassword(senha.getText().toString());

                    Call<TokenRequest> tokenResponseCall = service.getTokenAccess(tokenRequest.getEmail()
                            ,tokenRequest.getPassword());

                    tokenResponseCall.enqueue(new Callback<TokenRequest>() {
                        @Override
                        public void onResponse(Call<TokenRequest> call, Response<TokenRequest> response) {

                           TokenResponse tokenResponse = new TokenResponse();
                            Headers resp = response.headers();
                            tokenResponse.setAccess_token(resp.get("access-token"));
                            tokenResponse.setClient(resp.get("client"));
                            tokenResponse.setUid(resp.get("uid"));


                            if(response.code() == 200){

                                startActivity(new Intent(getApplicationContext(), Home.class));

                            }else if (response.code() == 401){

                                Toast.makeText(getApplicationContext()," Email ou senha incorretos. ",Toast.LENGTH_LONG).show();
                            }

                            SharedPreferences sp = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sp.edit();

                            editor.putString("access-token",tokenResponse.getAccess_token());
                            editor.putString("client",tokenResponse.getClient());
                            editor.putString("uid",tokenResponse.getUid());
                            editor.apply();

                        }

                        @Override
                        public void onFailure(Call<TokenRequest> call, Throwable throwable) {

                            Log.d("Status:", "onFailure: "+ throwable.getMessage());
                        }
                    });
                }
            }
        });

    }

    public boolean isEmail(String string){

        return string.contains("@");

    }
    private boolean isPassword(String string){

        return string.length() < 5;
    }
    public void onBackPressed(){

        finish();
    }
}
